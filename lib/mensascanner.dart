import 'dart:convert';
import 'dart:io';
import 'dart:async';
//import 'package:http/http.dart' as http;
import 'package:rpi_gpio/rpi_gpio.dart';
import 'package:webdav/webdav.dart';

var redLed, greenLed;
run() async {
  final gpio = RpiGpio();

  redLed = gpio.output(7); // GPIO 4
  greenLed = gpio.output(11); // GPIO 17

  redLed.value = false;
  greenLed.value = false;
  /*
  redLed.value = true;
  greenLed.value = true;
  await Future.delayed(Duration(seconds: 3));
  redLed.value = false;
  greenLed.value = false;
  */
//while(true){
  // await Future.delayed(Duration(seconds: 1));
//led.value=false;

  // await Future.delayed(Duration(seconds: 1));
//led.value=true;
//}
//gpio.dispose();

  print('Start Server...');

  // var r = await http.get(
  //    'https://firestore.googleapis.com/v1/projects/aesapp-48223/databases/(default)/documents/vertretung');

  // List newEntfall = [];
  // for (Map m in json.decode(r.body)['documents']) {
  //   if (m['fields']['entfall']['booleanValue'] ?? false) {
  //    newEntfall.add(m['fields']);
  //print(m['fields']);
  //print(m['fields']['klasse']);
  //print(m['fields']['stunde']);
  //   }
  // }
  //entfall.clear();
  //entfall.addAll(newEntfall);
  /* var fileContent = File('data/klassen.json').readAsStringSync();
  var klassenJson = json.decode(fileContent);

  for (String key in klassenJson.keys) {
    print(key);
    print(klassenJson[key]);
    klassen.add(Klasse(key, klassenJson[key]['from'], klassenJson[key]['to']));
  } */
  //print(entfall);

  mensaNummern =
      json.decode(File('data/ids.json').readAsStringSync()).cast<Map>();

  /* futureDelayed(int scanTime) async {
    print('Future start');
    await Future.delayed(const Duration(seconds: 3));
    exit(0);
    print('Future completed');
    print('$lastScanTime == $scanTime');
    if (lastScanTime == scanTime) {
      // redLed.value = false;
      //greenLed.value = false;
    }
  } */

  StreamSubscription sub =
      Stream.periodic(Duration(minutes: 60 /* seconds: 30 */))
          .listen((_) => updateData());

  listen();
}

int lastScanTime = now.millisecondsSinceEpoch;
listen() async {
  while (true) {
    var s = stdin.readLineSync(retainNewlines: true);
    State res = process(s);
    if (res == State.approved) {
      greenLed.value = true;
    } else {
      redLed.value = true;
    }

    lastScanTime = now.millisecondsSinceEpoch;
    int scanTime = lastScanTime;
    print(lastScanTime);
    print(scanTime);
    print(res);
    await Future.delayed(const Duration(seconds: 3));
    redLed.value = false;
    greenLed.value = false;
  }
}

updateData() async {
  print('Update data');
  Map data = json.decode(File('data/credentials.json').readAsStringSync());
  Client webdavClient = Client(
      data['host'], data['username'], data['password'], data['path'],
      port: 443, protocol: 'https');
  await webdavClient.download('/mensascanner/ids.json', 'data/ids.json');
  await Future.delayed(Duration(seconds: 10));

  mensaNummern =
      json.decode(File('data/ids.json').readAsStringSync()).cast<Map>();
  print(mensaNummern.firstWhere((m) => m['nr'] == 0)['klasse']);
}

List<Map> mensaNummern;

//List<Klasse> klassen = [];

//List entfall = [];

DateTime get now => DateTime.now();

List blacklist = [
  '05A',
  '05B',
  '05C',
  '05D',
  '05E',
  '05F',
  '05G',
  '06A',
  '06B',
  '06C',
  '06D',
  '06E',
  '06F',
  '06G',
  '07A',
  '07B',
  '07C',
  '07D',
  '07E',
  '07F',
  '07G',
  '08A',
  '08B',
  '08C',
  '08D',
  '08E',
  '08F',
  '08G',
];

Map preAllow = {
  1: ["06A", "06B", "06C"],
  2: ["05A", "05B", "05C", "05D", "05E", "05F"],
  3: ["05A", "05B", "05C", "05D", "05E", "05F"],
  4: ["08A", "08B", "08C"],
  5: ["07A", "07B", "07C"],
};

State process(String s) {
  try {
    var id = int.tryParse(s.substring(0, s.length - 1));

    /*   print(id);
    if (id == null || id < 9146000 || id > 9160000) {
      return State.invalid;
    }
    Klasse k = klassen.firstWhere((k) => k.from <= id && k.to >= id);
    print(k); */
    if (id == null) {
      return State.invalid;
    }

    String klasse;
    try {
      klasse = mensaNummern.firstWhere((m) => m['nr'] == id)['klasse'];
    } catch (e) {
      return State.invalid;
    }
    print(klasse);

    if (now.hour >= 12) {
      return State.approved;
    }
    String k;
    try {
      k = blacklist.firstWhere((s) => klasse.contains(s));
    } catch (e) {
      return State.approved;
    }

    if (preAllow[now.weekday].contains(k)) {
      return State.approved;
    }
    print(k);
    print(now.weekday);
    return State.denied;
  } catch (e, st) {
    print(e);
    print(st);
    return State.error;
  }
  return State.denied;
}

enum State { approved, denied, invalid, error }
/*
class Klasse {
  String name;
  int from;
  int to;
  Klasse(this.name, this.from, this.to);

  toString() => name;
}

class Time {
  int hour, minute;
  Time(this.hour, this.minute);
}
*/
